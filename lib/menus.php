<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'mobile_navigation' => __( 'Mobile Menu' ),
		'utility_navigation' => __( 'Utility Menu' ),
		'about_navigation' => __( 'About Menu' ),
		'products_navigation' => __( 'Products Menu' ),
		'training_navigation' => __( 'Training Menu' ),
		'additional_navigation' => __( 'Additional Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );