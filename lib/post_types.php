<?php
/*----------------------------------------------------------------*\
	INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Register Custom Post Type: Testimony
function create_testimony_cpt() {
	$labels = array(
		'name' => _x( 'Testimonies', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Testimony', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Testimonies', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Testimony', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Testimony Archives', 'textdomain' ),
		'attributes' => __( 'Testimony Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Testimony:', 'textdomain' ),
		'all_items' => __( 'All Testimonies', 'textdomain' ),
		'add_new_item' => __( 'Add New Testimony', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Testimony', 'textdomain' ),
		'edit_item' => __( 'Edit Testimony', 'textdomain' ),
		'update_item' => __( 'Update Testimony', 'textdomain' ),
		'view_item' => __( 'View Testimony', 'textdomain' ),
		'view_items' => __( 'View Testimonies', 'textdomain' ),
		'search_items' => __( 'Search Testimony', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Testimony', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimony', 'textdomain' ),
		'items_list' => __( 'Testimonies list', 'textdomain' ),
		'items_list_navigation' => __( 'Testimonies list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Testimonies list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Testimony', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-editor-quote',
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'testimony', $args );
}
add_action( 'init', 'create_testimony_cpt', 0 );

function set_posts_per_page_for_towns_cpt( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'testimony' ) ) {
    $query->set( 'posts_per_page', '12' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_towns_cpt' );
// Register Custom Post Type Tutor
function create_tutor_cpt() {
	$labels = array(
		'name' => _x( 'Tutors', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Tutor', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Tutors', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Tutor', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Tutor Archives', 'textdomain' ),
		'attributes' => __( 'Tutor Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Tutor:', 'textdomain' ),
		'all_items' => __( 'All Tutors', 'textdomain' ),
		'add_new_item' => __( 'Add New Tutor', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Tutor', 'textdomain' ),
		'edit_item' => __( 'Edit Tutor', 'textdomain' ),
		'update_item' => __( 'Update Tutor', 'textdomain' ),
		'view_item' => __( 'View Tutor', 'textdomain' ),
		'view_items' => __( 'View Tutors', 'textdomain' ),
		'search_items' => __( 'Search Tutor', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Tutor', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Tutor', 'textdomain' ),
		'items_list' => __( 'Tutors list', 'textdomain' ),
		'items_list_navigation' => __( 'Tutors list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Tutors list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Tutor', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'tutor', $args );
}
add_action( 'init', 'create_tutor_cpt', 0 );