<?php
/*----------------------------------------------------------------*\
	DECLARE WOOCOMMERCE SUPPORT
\*----------------------------------------------------------------*/
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'thumbnail_image_width' => 300,
		'single_image_width'    => 800,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 6,
            'default_columns' => 3,
            'min_columns'     => 1,
            'max_columns'     => 3,
        ),
	) );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
/*----------------------------------------------------------------*\
	REMOVE SPECIFIC STYLES
\*----------------------------------------------------------------*/
function dequeue_woo_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	// unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	// unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}
add_filter( 'woocommerce_enqueue_styles', 'dequeue_woo_styles' );
/*----------------------------------------------------------------*\
	SUPPORT GALLERY FEATURES
\*----------------------------------------------------------------*/
// add_theme_support( 'wc-product-gallery-zoom' );
// add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
/*----------------------------------------------------------------*\
	BREADCRUMBS BELOW TITLE
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
add_action( 'woocommerce_archive_description','woocommerce_breadcrumb', 1, 0);
/*----------------------------------------------------------------*\
	REMOVE RESULT COUNT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
/*----------------------------------------------------------------*\
	REMOVE SPECIFIC SORT OPTIONS
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_catalog_orderby', 'remove_sorting_options' );
function remove_sorting_options( $options ) {
	 unset( $options['rating'] );   
	 unset( $options['popularity'] );   
   return $options;
}
/*----------------------------------------------------------------*\
	MOVE SORTING TO ADJACENT SIBLING TO BREADCRUMBS
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_action( 'woocommerce_archive_description', 'woocommerce_catalog_ordering', 20 );
/*----------------------------------------------------------------*\
	CHANGE SALE TAG TEXT
\*----------------------------------------------------------------*/
add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product) {
	return '<span class="onsale">sale</span>';
}
/*----------------------------------------------------------------*\
	ADD SKU TO PRODUCT PREVIEWS
\*----------------------------------------------------------------*/
function shop_display_skus() {
	global $product;
	if ( $product->get_sku() ) {
		echo '<div class="product-meta">SKU: ' . $product->get_sku() . '</div>';
	}
}
add_action( 'woocommerce_after_shop_loop_item_title', 'shop_display_skus', 9 );
/*----------------------------------------------------------------*\
	REMOVE ADD-TO-CART BUTTON FROM PRODUCT PREVIEWS
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
/*----------------------------------------------------------------*\
	REMOVE TABS FROM SINGLE PRODUCT
\*----------------------------------------------------------------*/
function woo_remove_product_tabs( $tabs ) {
	unset( $tabs['description'] );          // Remove the description tab
	unset( $tabs['reviews'] );          // Remove the reviews tab
	unset( $tabs['additional_information'] );   // Remove the additional information tab
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
/*----------------------------------------------------------------*\
	MOVE DESCRIPTION BELOW PRICE FOR SINGLE PRODUCT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
function add_full_description() {
	global $post;
	echo '<div class="product-post-content">' . the_content() . '</div>';
}
add_action( 'woocommerce_single_product_summary', 'add_full_description', 25 );
/*----------------------------------------------------------------*\
	REMOVE LINKS FROM GALLERY
\*----------------------------------------------------------------*/
function remove_link_on_thumbnails( $html ) {
	return strip_tags( $html,'<div><img>' );
}
add_filter('woocommerce_single_product_image_thumbnail_html','remove_link_on_thumbnails' );
/*----------------------------------------------------------------*\
	REMOVE META FROM SINGLE PRODUCT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
/*----------------------------------------------------------------*\
	MOVE SKU BELOW TITLE FOR SINGLE PRODUCT
\*----------------------------------------------------------------*/
function sku_below_title(){
	global $product;
	echo 'SKU: '.$product->get_sku();
}
add_action( 'woocommerce_single_product_summary', 'sku_below_title', 8 );
/*----------------------------------------------------------------*\
	MOVE SALE TAG ABOVE TITLE ON SINGLE PRODUCT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 5 );
/*----------------------------------------------------------------*\
	# OF RELATED PRODUCTS
\*----------------------------------------------------------------*/
function woo_related_products_limit() {
  global $product;
	$args['posts_per_page'] = 4;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'woo_related_products_limit', 20 );
/*----------------------------------------------------------------*\
	REMOVE COUPON FROM CHECKOUT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 ); 
/*----------------------------------------------------------------*\
	REMOVE DASHBOARD
\*----------------------------------------------------------------*/
// remove from menu
add_filter( 'woocommerce_account_menu_items', 'misha_remove_my_account_dashboard' );
function misha_remove_my_account_dashboard( $menu_links ){
	unset( $menu_links['dashboard'] );
	return $menu_links;
}
/*----------------------------------------------------------------*\
	ADD ADDITIONAL LINKS TO MY ACCOUNT
\*----------------------------------------------------------------*/
add_filter ( 'woocommerce_account_menu_items', 'additional_links' );
function additional_links( $menu_links ){
	// or in case you need 2 links
	$new = array('training' => 'Training Attendance', 'interactive' => 'Access Interactive OG', 'digital' => 'Digital Downloads', );
 
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 4, true ) 
	+ $new 
	+ array_slice( $menu_links, 1, NULL, true );
 
	return $menu_links;
}
 
add_filter( 'woocommerce_get_endpoint_url', 'additional_hooks', 10, 4 );
function additional_hooks( $url, $endpoint, $value, $permalink ){
 
	if( $endpoint === 'training' ) {
		$url = 'https://admin.imse.com/login/';
	}
	if( $endpoint === 'interactive' ) {
		$url = 'https://interactive.imse.com/login.htm';
	}
	if( $endpoint === 'digital' ) {
		$url = 'https://login.vitrium.com/';
	}
	return $url;
 
}
/*----------------------------------------------------------------*\
	TAX EXCEMPT QUESTION
\*----------------------------------------------------------------*/
function qd_tax_exempt( $checkout ) {
	echo '<h3>Additional Order Details</h3>';
	echo '<div class="tax-exempt-option">';
	woocommerce_form_field( 'shipping_method_tax_exempt', 
		array(
			'type' => 'radio',
			'class' => array( 'form-row-wide', 'tax-exempt' ),
			'options' => array(
				'2' => 'Yes',
				'1' => 'No',
			),
      'label' => __('Should this order be tax exempt?'),
      'required'  => true,
			), $checkout->get_value( 'shipping_method_tax_exempt' )
		);
  echo '</div>';
}
add_action( 'woocommerce_before_order_notes', 'qd_tax_exempt');
// Remove Taxes
add_action( 'woocommerce_checkout_update_order_review', 'taxexempt_checkout_update_order_review');
function taxexempt_checkout_update_order_review( $post_data ) {
  global $woocommerce;
  $woocommerce->customer->set_is_vat_exempt(FALSE);
  parse_str($post_data);
  if ( isset($shipping_method_tax_exempt) && $shipping_method_tax_exempt == '2')
    $woocommerce->customer->set_is_vat_exempt(true);                
}
// Validate
function customised_checkout_field_process() {
	// Show an error message if the field is not set.
	if (!$_POST['shipping_method_tax_exempt']) wc_add_notice(__('Confirm if this order is <b>tax exempt</b> or not.') , 'error');
}
add_action('woocommerce_checkout_process', 'customised_checkout_field_process');
/*----------------------------------------------------------------*\
	CASH ON DELIVERY / PO QUOTE SET TO ON-HOLD
\*----------------------------------------------------------------*/
function action_woocommerce_thankyou_cod($order_id) {
	$order = wc_get_order($order_id);
	$order->update_status('on-hold');
}
add_action('woocommerce_thankyou_cod', 'action_woocommerce_thankyou_cod', 10, 1);
/*----------------------------------------------------------------*\
	ORDER TAHNK YOU TITLE UPDATED
\*----------------------------------------------------------------*/
function title_order_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = "Order Approved";
	}
	return $title;
}
add_filter( 'the_title', 'title_order_received', 10, 2 );
/*----------------------------------------------------------------*\
	ADD SHIPPING FOR VIRTUAL PRODUCTS
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_cart_needs_shipping_address', '__return_true', 50 );
/*----------------------------------------------------------------*\
	SHIPPING AUTO OPENED
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
/*----------------------------------------------------------------*\
	Separate WooCommerce Login
\*----------------------------------------------------------------*/
add_shortcode( 'wc_login_form', 'separate_login_form' );
function separate_login_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return; 
   ob_start();
   woocommerce_login_form( array( 'redirect' => '/my-account' ) );
   return ob_get_clean();
}
/*----------------------------------------------------------------*\
	Separate WooCommerce Customer Registration
\*----------------------------------------------------------------*/
add_shortcode( 'wc_reg_form', 'separate_registration_form' );
function separate_registration_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return;
   ob_start();
   // NOTE: THE FOLLOWING <FORM></FORM> IS COPIED FROM woocommerce\templates\myaccount\form-login.php
   // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY
   do_action( 'woocommerce_before_customer_login_form' );
   ?>
      <form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
         <?php do_action( 'woocommerce_register_form_start' ); ?>
         <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
               <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
               <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
            </p>
         <?php endif; ?>
         <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
         </p>
         <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
               <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
               <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
            </p>
         <?php else : ?>
            <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>
         <?php endif; ?>
         <?php do_action( 'woocommerce_register_form' ); ?>
         <p class="woocommerce-FormRow form-row">
            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
            <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
         </p>
         <?php do_action( 'woocommerce_register_form_end' ); ?>
      </form>
   <?php
   return ob_get_clean();
}