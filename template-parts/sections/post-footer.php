<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<div>
			<div>
				<a href="<?php echo get_home_url(); ?>">
					<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse.svg" alt="institute for multi-sensory education" />
				</a>
				<address>
					<a href="tel:+18006469788" class="button is-text">(800) 646 - 9788 ></a><br/>
					24800 Denso Drive<br/>
					Suite 202<br/>
					Southfield, MI 48033<br/>
				</address>
			</div>
			<details>
				<summary>About</summary>
				<nav>
					<?php wp_nav_menu(array( 'theme_location' => 'about_navigation' )); ?>
				</nav>
			</details>
			<details>
				<summary>Products</summary>
				<nav>
					<?php wp_nav_menu(array( 'theme_location' => 'products_navigation' )); ?>
				</nav>
			</details>
			<details>
				<summary>Training</summary>
				<nav>
					<?php wp_nav_menu(array( 'theme_location' => 'training_navigation' )); ?>
				</nav>
			</details>
			<details>
				<summary>Additional Links</summary>
				<nav>
					<?php wp_nav_menu(array( 'theme_location' => 'additional_navigation' )); ?>
				</nav>
			</details>
			<div>
				<span class="title">Follow Us</span>
				<nav>
					<ul class="social-nav">
						<li>
							<a target="_blank" href="https://www.facebook.com/Orton-Gillingham-358905134128313/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-fb.svg" alt="institute for multi-sensory education on facebook" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.pinterest.com/ortongillingham/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-pinterest.svg" alt="institute for multi-sensory education on pinterest" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.youtube.com/channel/UCPJqkhSNKynPzgLj4Xuqp9g?view_as=subscriber">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-youtube.svg" alt="institute for multi-sensory education on youtube" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://twitter.com/IMSEOG">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-twitter.svg" alt="institute for multi-sensory education on twitter" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.linkedin.com/company/orton-gillingham/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-linked.svg" alt="institute for multi-sensory education on linkedin" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.instagram.com/imse_og/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-insta.svg" alt="institute for multi-sensory education on instagram" />
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<div>
			<div class="login">
				<a href="" class="button is-text">Member Login ></a>
				<a href="" class="button is-text split">
					<span class="label">Not a member?</span>
					<span class="link">Sign up ></span>
				</a>
			</div>
			<div class="badges">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-ida.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo_WBENC_seal.png" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-reading-leage.png" />
			</div>
		</div>
	</div>
</footer>
<footer class="copyright">
	<div>
		<div>
			<p>© <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<ul>
				<li>
					<a href="<?php the_permalink(323); ?>">Privacy Policy</a>
				</li>
				<li>
					<a href="<?php the_permalink(2172); ?>">Terms & Conditions</a>
				</li>
			</ul>
		</div>
		<a target="_blank" href="https://element5digital.com" rel="nofollow">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit_alt.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
</footer>