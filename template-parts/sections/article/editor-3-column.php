<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a 3 wysiwyg editor

\*----------------------------------------------------------------*/
?>

<section class="editor-3-column <?php the_sub_field('width'); ?>">
	<div>
		<?php the_sub_field('content-left'); ?>
	</div>
	<div>
		<?php the_sub_field('content-center'); ?>
	</div>
	<div>
		<?php the_sub_field('content-right'); ?>
	</div>
</section>