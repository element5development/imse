<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying lastest testimony is a slider format

\*----------------------------------------------------------------*/
?>
<?php $posts = get_sub_field('testimonies'); ?>

<?php if( $posts ): ?>
	<section class="testimonies <?php the_sub_field('width'); ?>">
		<h2>What other teachers are saying about IMSE training</h2>
		<ul>
			<?php $i=0; foreach( $posts as $post): $i++; // variable must be called $post (IMPORTANT) ?>
			<li id="testimony-<?php echo $i; ?>" class="testimony <?php if ( $i == 2 ) : ?>is-active<?php endif; ?>">
				<?php
					$quote = get_field('quote');
					if ( strlen($quote) >= 450 ) : 
						$quote = substr($quote, 0, 450) . '..."';
					endif;
				?>
				<p><?php echo $quote; ?></p>
				<button>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logomark.svg" />
					<div>
						<p><?php the_title(); ?></p>
						<?php if ( get_field('job_title') ) : ?>
							<p>
								<b><?php the_field('job_title'); ?></b>
								<?php if ( get_field('location') ) : ?>
									<br/><?php the_field('location'); ?>
								<?php endif; ?>
							</p>
						<?php endif; ?>
					</div>
				</button>
				<?php 
					$prev = $i - 1;
					if ( $prev == 0 ) : 
						$prev = 3;
					endif;
					$next = $i + 1;
					if ( $next == 4 ) : 
						$next = 1;
					endif;
				?>
				<a href="#testimony-<?php echo $prev; ?>" class="arrow prev"></a>
				<a href="#testimony-<?php echo $next; ?>" class="arrow next"></a>
			</li>
			<?php endforeach; ?>
		</ul>
	</section>
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>