<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$images = get_sub_field('gallery');
	$columns = get_sub_field('columns');
?>

<section class="gallery <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
	<?php foreach( $images as $image ): ?>
		<figure>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		</figure>
	<?php endforeach; ?>
</section>
