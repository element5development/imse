<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with an editor

\*----------------------------------------------------------------*/
?>

<section class="media-text <?php the_sub_field('width'); ?> <?php the_sub_field('image_alignment'); ?> <?php the_sub_field('image_size') ?>">
	<div>
		<?php $image = get_sub_field('image'); ?>
		<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
	</div>
	<div>
		<?php the_sub_field('content'); ?>
	</div>
</section>