<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying grid of cards

\*----------------------------------------------------------------*/
?>

<?php //GALLERY
	$columns = get_sub_field('columns');
	$format = get_sub_field('format')
?>

<section class="card-grid <?php the_sub_field('format'); ?>-cards <?php the_sub_field('width'); ?> columns-<?php echo $columns; ?>">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="card">
			<!-- IMAGE -->
			<?php $image = get_sub_field('image'); ?>
			<?php if ( get_sub_field('image') && $format == 'image-top' ) : ?>
				<figure>
					<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				</figure>
			<?php endif; ?>
			<!-- HEADLINE -->
			<?php if ( get_sub_field('title') ) : ?>
				<h2><?php the_sub_field('title') ?></h2>
			<?php endif; ?>
			<!-- DESCRIPTION -->
			<div class="description">
				<?php if ( get_sub_field('description') ) : ?>
					<?php the_sub_field('description'); ?>
				<?php endif; ?>
				<!-- CONTENT -->	
				<?php if ( get_sub_field('contents') && $format == 'image-top' ) : ?>
					<?php the_sub_field('contents'); ?>
				<?php endif; ?>
			</div>	
			<!-- BUTTON -->
			<?php if ( get_sub_field('button') && get_sub_field('button_two')  ) : ?>
				<div>
					<?php
						if ( get_sub_field('button') ) : 
						$link = get_sub_field('button'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
					?>
						<a class="button <?php if ($format == 'image-top') : ?>is-red<?php endif ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
					<!-- BUTTON -->
					<?php
						if ( get_sub_field('button_two') ) : 
						$link = get_sub_field('button_two'); 
						$link_url_two = $link['url'];
						$link_title_two = $link['title'];
						$link_target_two = $link['target'] ? $link['target'] : '_self'; 
					?>
						<a class="button <?php if ($format == 'text') : ?>is-text<?php endif ?>" href="<?php echo esc_url($link_url_two); ?>" target="<?php echo esc_attr($link_target_two); ?>">
							<?php echo esc_html($link_title_two); ?> >
						</a>
					<?php endif; ?>
				</div>
			<?php else : ?>
				<?php if ( get_sub_field('button') || get_sub_field('button_two')  ) : ?>
					<?php
						if ( get_sub_field('button') ) : 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						elseif ( get_sub_field('button_two') ) :
							$link = get_sub_field('button_two'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						endif;
					?>
					<a class="cover-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"></a>
					<div>
						<div class="button <?php if ($format == 'image-top') : ?>is-red<?php endif ?>">
							<?php echo esc_html($link_title); ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	<?php endwhile; ?>
</section>