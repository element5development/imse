<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single wysiwyg editor

\*----------------------------------------------------------------*/
?>

<section class="editor <?php the_sub_field('width'); ?>">
	<?php the_sub_field('content'); ?>
</section>