<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="utility">
	<div>
		<nav class="<?php if ( WC()->cart->get_cart_contents_count() != 0 ) : ?>has-items<?php endif; ?>">
			<?php wp_nav_menu(array( 'theme_location' => 'utility_navigation' )); ?>
			<button class="search">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-search.svg" alt="search site" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-close.svg" alt="close search form" />
			</button>
		</nav>	
		<?php echo get_search_form(); ?>
	</div>
</div>

<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse-bg.svg" alt="institute for multi-sensory education" />
		</a>
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="/my-account" class="button">My Account</a>
			<?php else : ?>
				<a href="/account" class="button">Login</a>
			<?php endif; ?>
			<a href="tel:+18006469788">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-call.svg" alt="call imse" />
			</a>
		</div>
	</nav>
</div>

<div class="sticky-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse.svg" alt="institute for multi-sensory education" />
		</a>
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
			<a class="phone" href="tel:+18006469788">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-call.svg" alt="call imse" />
			</a>
			<a class="cart <?php if ( WC()->cart->get_cart_contents_count() != 0 ) : ?>has-items<?php endif; ?>" href="<?php echo wc_get_cart_url(); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-cart.svg" alt="view cart" />
			</a>
		</div>
	</nav>
</div>

<?php get_template_part('template-parts/elements/navigation-mobile'); ?>