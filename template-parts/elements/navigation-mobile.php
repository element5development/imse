<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION FOR MOBILE DEVICES

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation-mobile">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse-bg.svg" alt="institute for multi-sensory education" />
		</a>
		<div>
			<a href="tel:+18442923249">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-call-mobile.svg" alt="call imse" />
			</a>
			<a href="<?php echo wc_get_cart_url(); ?>" class="<?php if ( WC()->cart->get_cart_contents_count() != 0 ) : ?>has-items<?php endif; ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-cart-mobile.svg" alt="view cart" />
			</a>
			<button class="mobile-menu">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-menu-mobile.svg" alt="open menu" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-close-mobile.svg" alt="close menu" />
			</button>
		</div>
	</nav>
</div>

<div class="sticky-navigation-mobile">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse.svg" alt="institute for multi-sensory education" />
		</a>
		<div>
			<a href="">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-call-mobile.svg" alt="call imse" />
			</a>
			<a href="">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-cart-mobile.svg" alt="view cart" />
			</a>
			<button class="mobile-menu">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-menu-mobile.svg" alt="open menu" />
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-close-mobile.svg" alt="close menu" />
			</button>
		</div>
	</nav>
</div>

<div class="active-mobile-menu">
	<div class="search">
		<?php echo get_search_form(); ?>
	</div>
	<nav>
		<?php wp_nav_menu(array( 'theme_location' => 'mobile_navigation' )); ?>
	</nav>
	<div class="login">
		<?php if ( is_user_logged_in() ) : ?>
			<a href="/my-account" class="button">My Account</a>
		<?php else : ?>
			<a href="/account" class="button">Login</a>
		<?php endif; ?>
	</div>
</div>