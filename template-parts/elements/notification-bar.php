<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<?php if ( get_field('site_notification', 'option') ) : ?>
	<div class="standard-notification notification-bar note-on">
		<div> 
			<button class="is-icon"> 
				<svg><use xlink:href="#close"></use></svg> 
			</button>
			<p><?php the_field('site_notification', 'option'); ?></p>
		</div>
	</div>
<?php endif; ?>