<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<div class="cookie-useage-notification notification-bar note-on">
	<div> 
		<p>This site uses cookies to provide you with a greater user experience. By using our website, you accept our <a href="<?php echo get_privacy_policy_url(); ?>">use of cookies</a>.</p>
		<button> 
			I accept
		</button>
	</div>
</div>