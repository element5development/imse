<?php 
/*----------------------------------------------------------------*\

	Template Name: Training Archive 
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/notification-bar'); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<nav class="child-page-navigation">
	<?php wp_nav_menu(array( 'theme_location' => 'training_navigation' )); ?>
</nav>

<main id="main-content">
	<article>
		<?php 
		/*----------------------------------------------------------------*\
		|
		| Insert page content which is most often handled via ACF Pro
		| and highly recommend the use of the flexiable content so
		|	we already placed that code here.
		|
		| https://www.advancedcustomfields.com/resources/flexible-content/
		|
		\*----------------------------------------------------------------*/
		?>
		<?php
			while ( have_rows('article') ) : the_row();
				if( get_row_layout() == 'editor' ):
					get_template_part('template-parts/sections/article/editor');
				elseif( get_row_layout() == '2editor' ):
					get_template_part('template-parts/sections/article/editor-2-column');
				elseif( get_row_layout() == '3editor' ):
					get_template_part('template-parts/sections/article/editor-3-column');
				elseif( get_row_layout() == 'media+text' ):
					get_template_part('template-parts/sections/article/media-text');
				elseif( get_row_layout() == 'sidebar+text' ):
					get_template_part('template-parts/sections/article/sidebar-text');
				elseif( get_row_layout() == 'cover' ):
					get_template_part('template-parts/sections/article/cover');
				elseif( get_row_layout() == 'gallery' ):
					get_template_part('template-parts/sections/article/gallery');
				elseif( get_row_layout() == 'card_grid' ):
					get_template_part('template-parts/sections/article/card-grid');
				elseif( get_row_layout() == 'testimonies' ):
					get_template_part('template-parts/sections/article/testimonies');
				elseif( get_row_layout() == 'price_card' ):
					get_template_part('template-parts/sections/article/price-card');
				endif;
			endwhile;
		?>

		<?php
			$date=date("Ymd");
			//$date='20170101';
			$result = file_get_contents('https://shop.imse.com/api/availableSessions/'.$date);
			//echo 'https://shop-qa.imse.com/api/availableSessions/'.$date;
			// Will dump a beauty json :3
			$courseData = json_decode($result, true);
			function date_compare($a, $b) {
				$t1 = strtotime($a['start_date']);
				$t2 = strtotime($b['start_date']);
				return $t1 - $t2;
			}    
			usort($courseData, 'date_compare');
			$city=array();
			$state=array();
			$course=array();
			foreach ($courseData as $key => $value) {
			if(isset($_GET['state']) && $_GET['state']!=''){
				if($_GET['state'] == $value['state']){
					if (!in_array($value['city'], $city)){
						array_push($city,$value['city']);
					}
				}
			} else {
				if (!in_array($value['city'], $city)){
					array_push($city,$value['city']);
					}
				}
				if (!in_array($value['state'], $state)){
					array_push($state,$value['state']);
				}
				if (!in_array($value['title'], $course)){
					array_push($course,$value['title']);
				}
			}
			sort($state);
			sort($city);
			sort($course);
			$cF=0;
			$sF=0;
			$cuF=0;
			if(isset($_GET['city']) && $_GET['city']!=''){
				$cF=1;
			}
			if(isset($_GET['state']) && $_GET['state']!=''){
				$sF=1;
			}
			if(isset($_GET['course']) && $_GET['course']!=''){
				$cuF=1;
			}
		?>
		<section class="main-section-1 is-extra-wide"> 
			<h2>Training Options</h2>
			<div class="sec1-div2">
				<form id="form1">
					<div class="frm1">
						<label for="cars">Choose State</label>
						<select id="state" onchange="fnFilter('state',this.value);">
							<option value="" disabled selected>Select State</option>
							<?php foreach($state as $c){ ?>
								<option value="<?php echo $c; ?>" <?php if($c == $_GET['state']){ echo 'selected'; } ?>><?php echo $c; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="frm2">
						<label for="cars">Choose City</label>
						<select id="city" onchange="fnFilter('city',this.value);"> 
							<option value="" disabled selected>Select City</option>
							<?php foreach($city as $c){ ?>
								<option value="<?php echo $c; ?>" <?php if($c == $_GET['city']){ echo 'selected'; } ?>><?php echo $c; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="frm3">
						<label for="cars">Choose Course</label>
						<select id="course" onchange="fnFilter('course',this.value);">
							<option value="" disabled selected>Select Course Type</option>
							<?php foreach($course as $c){ ?>
								<option value="<?php echo $c; ?>" <?php if($c == $_GET['course']){ echo 'selected'; } ?>><?php echo $c; ?></option>
							<?php } ?>
						</select>
					</div>
				</form>
			</div>
			<div class="sec1-div3" id="oDiv">
				<div class="inner-table-div">
					<ul class="training-sechdule" id="oTable">
						<li>
							<div>Title</div>
							<div>Date</div>
							<div>City</div>
							<div>State</div>
							<div>Availability</div>
						</li>
						<?php foreach ($courseData as $key => $value) { ?>
							<?php //FILTER LOGIC
								$printC=1;
								$printS=1;
								$printCu=1;
								if( $cF==1 ) { //CITY FILTER IS SET
									if(urldecode($_GET['city']) != $value['city']) { //city doesnt match
										$printC=0;
									}
								}
								if( $sF==1 ) { //STATE FILTER IS SET
									if(urldecode($_GET['state']) != $value['state']) { //state doesnt match
										$printS=0;
									}
								}
								if( $cuF==1 ) { // COURSE FILTER IS SET
									if(urldecode($_GET['course']) != $value['title']) { //course doesnt match
										$printCu=0;
									}
								}
							?>
							<?php if($printC==1 && $printS==1 && $printCu == 1){ ?>
								<?php //SET CLASS BASED ON COURSE TYPE
									if ( strpos($value['title'], 'Comprehensive') !== false )  :
										$class="is-comprehensive";
									elseif ( strpos($value['title'], 'Intermediate') !== false ) :
										$class="is-intermediate";
									else : 
										$class="is-undefined";
									endif;
								?>

								<?php 
									$sdate = date_create($value['start_date']);
									$edate = date_create($value['end_date']);
									$fdate = date_format($sdate, 'M d') .' - '. date_format($edate, 'M d, Y');
								?>
								<a href="<?php echo add_query_arg( array('cid' => $value['session_id'],'date' => $fdate), 'single-training' );?>">
									<li class="<?php echo $class; ?>">
										<div>
											<?php echo $value['title']; ?>
										</div>
										<div>
											<?php echo $fdate; ?>
										</div>
										<div>
											<?php echo $value['city']; ?>
										</div>
										<div>
											<?php echo $value['state']; ?>
										</div>
										<div>
											<?php 
												if($value['available_seats']>0){
													echo '<span class="available">Available</span>'; 
												}else{
													echo '<span class="wait">Waitlist</span>';
												}
											?>
										</div>
									</li>
								</a>
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
			</div>
		</section>

		<script>
			var loc = '<?php echo get_site_url(); ?>/training/';
			var city='';
			var state='';
			var course='';
			function fnFilter(param,val){
				if(param=='city'){
					city=val;
					<?php if(isset($_GET['state']) && $_GET['state'] != ''){
					      ?>
						  state="<?php echo $_GET['state']; ?>";
						  <?php
					}
					?>
				}else if(param=='state'){
				state=val;
				}else{
					course=val;
				}
				window.location.href=loc+"?city="+city+"&state="+state+"&course="+course;
				/*if(window.location.href.indexOf('?') == -1){
				window.location.href=window.location.href+"?"+param+"="+val;
				}else{
				window.location.href=window.location.href+"&"+param+"="+val;
				}*/
			}
		</script>

	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>