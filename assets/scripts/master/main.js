var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		NOTIFICATION BARS
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	if (readCookie('standardNotification') === 'false') {
		$('.standard-notification').removeClass("note-on");
	} else {
		$('.standard-notification').addClass("note-on");
	}
	$('.standard-notification button').click(function () {
		$('.standard-notification').removeClass("note-on");
		createCookie('standardNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		TOGGLE SEARCH
	\*----------------------------------------------------------------*/
	$('button.search').on('click', function () {
		$(this).toggleClass('form-is-open');
		$('.utility form').toggleClass('is-open');
		$('.utility form input').focus();
	});
	/*----------------------------------------------------------------*\
		STICKY NAV
	\*----------------------------------------------------------------*/
	var lastScrollTop = 0;
	$(window).scroll(function (event) {
		var st = $(this).scrollTop();
		var scroll = $(window).scrollTop();
		if (scroll < 400 || st > lastScrollTop) {
			$('.sticky-navigation').removeClass('is-stuck');
			$('.sticky-navigation-mobile').removeClass('is-stuck');
		} else {
			$('.sticky-navigation').addClass('is-stuck');
			$('.sticky-navigation-mobile').addClass('is-stuck');
		}
		lastScrollTop = st;
	});
	/*----------------------------------------------------------------*\
		MOBILE NAV
	\*----------------------------------------------------------------*/
	$('button.mobile-menu').on('click', function () {
		$('html').toggleClass("cannot-scroll");
		$(this).toggleClass('is-opened');
		$('.active-mobile-menu').toggleClass('is-opened');
	});
	/*----------------------------------------------------------------*\
		REMOVE LINK FROM PARENT ITEMS
	\*----------------------------------------------------------------*/
	if ($(window).width() <= 800) {
		$("li.menu-item-has-children").children("a").attr('href', "javascript:void(0)");
		$('li.menu-item-has-children').on('click', function () {
			$(this).toggleClass('is-opened');
		});
	}
	/*----------------------------------------------------------------*\
		TOGGLE FOOTER DETAILS OPEN
	\*----------------------------------------------------------------*/
	if ($(window).width() >= 750) {
		$("footer.post-footer details").attr("open", "open");
	} else {
		$("footer.post-footer details").removeAttr("open");
	}
	$(window).resize(function () {
		if ($(window).width() >= 750) {
			$("footer.post-footer details").attr("open", "open");
		} else {
			$("footer.post-footer details").removeAttr("open");
		}
	});
	/*----------------------------------------------------------------*\
		TOGGLE TESTIMONEY
	\*----------------------------------------------------------------*/
	$('section.testimonies .testimony button').on('click', function () {
		$('.testimony').removeClass('is-active');
		$(this).parent('.testimony').addClass('is-active');
	});
	$('section.testimonies .testimony').on('click', function () {
		$('.testimony').removeClass('is-active');
		$(this).addClass('is-active');
	});
	/*----------------------------------------------------------------*\
		TUTOR LIST
	\*----------------------------------------------------------------*/
	if ($('.tutors').length) {
		var $grid = $('.tutors').isotope({
			itemSelector: '.tutor',
			layoutMode: 'fitRows',
			filter: '.MI',
			stagger: 30
		});
	}
	$('.filters-select').on('change', function () {
		var filterValue = this.value;
		$grid.isotope({
			filter: filterValue
		});
		// display message box if no filtered items
		if (!$grid.data('isotope').filteredItems.length) {
			$('#no-results').show();
		} else {
			$('#no-results').hide();
		}
	});
	/*----------------------------------------------------------------*\
		DOWNLOAD BUTTON LABEL
	\*----------------------------------------------------------------*/
	$("td.download-file .button").html("Download");
	/*----------------------------------------------------------------*\
		TRAINING SCHEDULE
	\*----------------------------------------------------------------*/
	$("table.training-sechdule tr").click(function () {
		window.location = $(this).data("href");
	});
	/*----------------------------------------------------------------*\
		RELOCATE PO FILE UPLOAD 
	\*----------------------------------------------------------------*/
	$("#alg_checkout_files_upload_form_1").insertBefore(".woocommerce-additional-fields__field-wrapper");
});