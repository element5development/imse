<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
	<article>
		<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
		<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part('template-parts/sections/article/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part('template-parts/sections/article/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part('template-parts/sections/article/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part('template-parts/sections/article/media-text');
					elseif( get_row_layout() == 'sidebar+text' ):
						get_template_part('template-parts/sections/article/sidebar-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part('template-parts/sections/article/cover');
					elseif( get_row_layout() == 'gallery' ):
						get_template_part('template-parts/sections/article/gallery');
					elseif( get_row_layout() == 'card_grid' ):
						get_template_part('template-parts/sections/article/card-grid');
					elseif( get_row_layout() == 'testimonies' ):
						get_template_part('template-parts/sections/article/testimonies');
					elseif( get_row_layout() == 'price_card' ):
						get_template_part('template-parts/sections/article/price-card');
					endif;
				endwhile;
			?>
	</article>
	<?php else : ?>
	<article>
		<section class="is-narrow">
			<h2>Uh Oh. Something is missing.</h2>
			<p>Looks like this page has no content.</p>
		</section>
	</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>