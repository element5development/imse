<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/notification-bar'); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<?php if ( is_page(468) || 468 == $post->post_parent ) : //About ?>
	<nav class="child-page-navigation">
		<?php wp_nav_menu(array( 'theme_location' => 'about_navigation' )); ?>
	</nav>
<?php elseif ( is_page(474) || 474 == $post->post_parent ) : //TRAINING ?>
	<nav class="child-page-navigation">
		<?php wp_nav_menu(array( 'theme_location' => 'training_navigation' )); ?>
	</nav>
<?php endif; ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
			<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part('template-parts/sections/article/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part('template-parts/sections/article/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part('template-parts/sections/article/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part('template-parts/sections/article/media-text');
					elseif( get_row_layout() == 'sidebar+text' ):
						get_template_part('template-parts/sections/article/sidebar-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part('template-parts/sections/article/cover');
					elseif( get_row_layout() == 'gallery' ):
						get_template_part('template-parts/sections/article/gallery');
					elseif( get_row_layout() == 'card_grid' ):
						get_template_part('template-parts/sections/article/card-grid');
					elseif( get_row_layout() == 'testimonies' ):
						get_template_part('template-parts/sections/article/testimonies');
					elseif( get_row_layout() == 'price_card' ):
						get_template_part('template-parts/sections/article/price-card');
					endif;
				endwhile;
			?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>

	<?php if ( is_page(488) || is_page(491) ) : //support and for parents ?>
		<?php
			// Custom WP query tutor_query
			$args_tutor_query = array(
				'post_type' => array('tutor'),
				'posts_per_page' => -1,
				'order' => 'DESC',
				'orderby' => 'title',
			); 
			$tutor_query = new WP_Query( $args_tutor_query );
		?>
		<?php if ( $tutor_query->have_posts() ) : ?>
			<section class="tutor-list standard">
				<label>Filter By State:</label>
				<select class="filters-select">
					<option value="*">View All</option>
					<option value=".AL">Alabama</option>
					<option value=".AK">Alaska</option>
					<option value=".AZ">Arizona</option>
					<option value=".AR">Arkansas</option>
					<option value=".CA">California</option>
					<option value=".CO">Colorado</option>
					<option value=".CT">Connecticut</option>
					<option value=".DC">District of Columbia</option>
					<option value=".DE">Delaware</option>
					<option value=".FL">Florida</option>
					<option value=".GA">Georgia</option>
					<option value=".HI">Hawaii</option>
					<option value=".ID">Idaho</option>
					<option value=".IL">Illinois</option>
					<option value=".IN">Indiana</option>
					<option value=".IA">Iowa</option>
					<option value=".KS">Kansas</option>
					<option value=".KY">Kentucky</option>
					<option value=".LA">Louisiana</option>
					<option value=".ME">Maine</option>
					<option value=".MD">Maryland</option>
					<option value=".MA">Massachusetts</option>
					<option value=".MI" selected>Michigan</option>
					<option value=".MN">Minnesota</option>
					<option value=".MS">Mississippi</option>
					<option value=".MO">Missouri</option>
					<option value=".MT">Montana</option>
					<option value=".NE">Nebraska</option>
					<option value=".NV">Nevada</option>
					<option value=".NH">New Hampshire</option>
					<option value=".NJ">New Jersey</option>
					<option value=".NM">New Mexico</option>
					<option value=".NY">New York</option>
					<option value=".NC">North Carolina</option>
					<option value=".ND">North Dakota</option>
					<option value=".OH">Ohio</option>
					<option value=".OK">Oklahoma</option>
					<option value=".OR">Oregon</option>
					<option value=".PA">Pennsylvania</option>
					<option value=".RI">Rhode Island</option>
					<option value=".SC">South Carolina</option>
					<option value=".SD">South Dakota</option>
					<option value=".TN">Tennessee</option>
					<option value=".TX">Texas</option>
					<option value=".UT">Utah</option>
					<option value=".VT">Vermont</option>
					<option value=".VA">Virginia</option>
					<option value=".WA">Washington</option>
					<option value=".WV">West Virginia</option>
					<option value=".WI">Wisconsin</option>
					<option value=".WY">Wyoming</option>
					<option value=".PR">Puerto Rico</option>
				</select>
				<div class="tutors">
					<?php while ( $tutor_query->have_posts() ) : $tutor_query->the_post(); ?>
						<div class="tutor <?php the_field('state'); ?>">
							<p class="name"><?php the_title(); ?></p>
							<p class="cert"><?php echo str_replace("-", "<br />", get_field('certification_level')); ?></p>
							<p class="location"><?php the_field('city'); ?>, <?php the_field('state'); ?></p>
							<p class="email"><a class="button is-text" href="mailto:<?php the_field('email'); ?>">Email <?php echo strtok(get_the_title(),  ' '); ?></a></p>
						</div>
					<?php endwhile; ?>
				</div>
				<div id="no-results">There are currently no certified tutors at this time. Please check back as we continually add new tutors to our database! In the meantime please check out Learning Ally for tutors in your area. <a target="_blank" href="www.learningally.org">www.learningally.org</a></div>
			</section>
		<?php endif; wp_reset_postdata(); ?>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>