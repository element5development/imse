<?php 
/*----------------------------------------------------------------*\

	ARCHIVE FOR CPT: TESTIMONY

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php $columns = get_field('testimony_header_columns','options'); ?>
	<header class="post-head columns-<?php echo $columns; ?>">
		<h1><?php the_title(); ?></h1>
		<?php if ( get_field('testimony_header_image','options') ) : ?>
			<?php $image = get_field('testimony_header_image','options'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
		<?php if ( get_field('testimony_header_image_two','options') && get_field('testimony_header_columns','options') > 1 ) : ?>
			<?php $image = get_field('testimony_header_image_two','options'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
		<?php if ( get_field('testimony_header_image_three','options') && get_field('testimony_header_columns','options') > 2 ) : ?>
			<?php $image = get_field('testimony_header_image_three','options'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
	</header>

<nav class="child-page-navigation">
	<?php wp_nav_menu(array( 'theme_location' => 'about_navigation' )); ?>
</nav>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="standard">
				<?php	while ( have_posts() ) : the_post(); ?>
					<blockquote>
						<p><?php the_field('quote'); ?></p>
						<footer class="quotee">
							<span class="name"><?php the_title(); ?></span>
							<?php if ( get_field('job_title') ) : ?>
								<strong><?php the_field('job_title'); ?></strong>
							<?php endif; ?>
							<?php if ( get_field('job_title') && get_field('location') ) : ?>
								, 
							<?php endif; ?>
							<?php if ( get_field('location') ) : ?>
								<?php the_field('location'); ?>
							<?php endif; ?>
						</footer>
					</blockquote>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>