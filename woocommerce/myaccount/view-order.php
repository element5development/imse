<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

defined( 'ABSPATH' ) || exit;

$notes = $order->get_customer_order_notes();
?>
<p>
<?php
printf(
	/* translators: 1: order number 2: order date 3: order status */
	esc_html__( 'Order #%1$s was placed on %2$s and is currently %3$s.', 'woocommerce' ),
	'<mark class="order-number">' . $order->get_order_number() . '</mark>', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	'<mark class="order-date">' . wc_format_datetime( $order->get_date_created() ) . '</mark>', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	'<mark class="order-status">' . wc_get_order_status_name( $order->get_status() ) . '</mark>' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
);
?>
</p>

<?php //determine if order has ticket product
	$cat_in_order = false;
	$items = $order->get_items();
	foreach ( $items as $item ) {      
		$product_id = $item->get_product_id();  
		if ( has_term( 'ticket', 'product_cat', $product_id ) ) {
				$cat_in_order = true;
				break;
		}
		}
?>
<?php if ( $cat_in_order == true ) : ?>
	<section class="register-attendees">
		<div>
			<h3>Attendee Registration</h3>
			<?php 
				global $current_user;
				get_currentuserinfo();
			?>
			<form id="attendee-registration" action="<?php echo get_home_url(); ?>/cart" method="post">
				<input type="hidden" id="userID" name="userID" value="<?php echo $current_user->ID; ?>">
				<input type="hidden" id="user-email" name="userEmail" value="<?php echo $current_user->user_email; ?>">
				<input type="hidden" id="order-number" name="orderNumber" value="<?php echo $order->get_order_number(); ?>">
				<label for="attendees">
					Attendee Names and Emails
					<span>Provide a name and email for each attendee, one per line.</span>
				</label>
				<textarea id="attendees" name="attendees" placeholder="John Doe, john@website.com&#10;Jane Doe, jane@website.com"></textarea>
				<p>
					<span>Example</span>
					John Doe, john@website.com<br>Jane Doe, jane@website.com
				</p>
				<button type="submit" value="Submit Attendees">Submit Attendees</button>
			</form>
		</div>
	</section>
<?php endif; ?>

<?php if ( $notes ) : ?>
	<h2><?php esc_html_e( 'Order updates', 'woocommerce' ); ?></h2>
	<ol class="woocommerce-OrderUpdates commentlist notes">
		<?php foreach ( $notes as $note ) : ?>
		<li class="woocommerce-OrderUpdate comment note">
			<div class="woocommerce-OrderUpdate-inner comment_container">
				<div class="woocommerce-OrderUpdate-text comment-text">
					<p class="woocommerce-OrderUpdate-meta meta"><?php echo date_i18n( esc_html__( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<div class="woocommerce-OrderUpdate-description description">
						<?php echo wpautop( wptexturize( $note->comment_content ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ol>
<?php endif; ?>

<?php do_action( 'woocommerce_view_order', $order_id ); ?>