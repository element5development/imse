<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
			
			<?php if ( $order->get_payment_method_title() == 'Quote for PO' ) : ?>
				<p class="payment-instructions">
					Once your purchase order is approved please email documentation to <a href="mailto:info@imse.com">info@imse.com</a> with the associated order number.
				</p>
			<?php elseif ( $order->get_payment_method_title() == 'Check Payment' ) : ?>
				<p class="payment-instructions">
					Please send a check to IMSE, 24800 Denso Drive Suite 202, Southfield, MI 48033
				</p>
			<?php endif; ?>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

			<?php //determine if order has ticket product
				$cat_in_order = false;
				$items = $order->get_items();
				foreach ( $items as $item ) {      
					$product_id = $item->get_product_id();  
					if ( has_term( 'ticket', 'product_cat', $product_id ) ) {
						 $cat_in_order = true;
						 break;
					}
				 }

				$IOG_in_order = false;
				$items = $order->get_items();
				foreach ( $items as $item ) {      
					$product_id = $item->get_product_id();  
					if ( $product_id == 1783 ) {
						 $IOG_in_order = true;
						 break;
					}
				 }
			?>
			<?php if ( $cat_in_order == true ) : ?>
				<section class="register-attendees">
					<div>
						<h3>Complete Training Registration</h3>
						<?php 
							global $current_user;
							get_currentuserinfo();
						?>
						<form id="attendee-registration" action="<?php echo get_home_url(); ?>/cart" method="post">
							<input type="hidden" id="userID" name="userID" value="<?php echo $current_user->ID; ?>">
							<input type="hidden" id="user-email" name="userEmail" value="<?php echo $current_user->user_email; ?>">
							<input type="hidden" id="order-number" name="orderNumber" value="<?php echo $order->get_order_number(); ?>">
							<label for="attendees">
								Attendee Names and Emails
								<span>Provide a name and email for each attendee, one per line.</span>
							</label>
							<textarea id="attendees" name="attendees" placeholder="John Doe, john@website.com&#10;Jane Doe, jane@website.com"></textarea>
							<p>
								<span>Example</span>
								John Doe, john@website.com<br>Jane Doe, jane@website.com
							</p>
							<button type="submit" value="Submit Attendees">Submit Attendees</button>
						</form>
					</div>
				</section>
			<?php endif; ?>
			<?php if ( $IOG_in_order == true ) : ?>
				<section class="register-attendees">
					<div>
						<h3>Complete Subscription Registration</h3>
						<?php 
							global $current_user;
							get_currentuserinfo();
						?>
						<form id="attendee-registration" action="<?php echo get_home_url(); ?>/cart" method="post">
							<input type="hidden" id="userID" name="userID" value="<?php echo $current_user->ID; ?>">
							<input type="hidden" id="user-email" name="userEmail" value="<?php echo $current_user->user_email; ?>">
							<input type="hidden" id="order-number" name="orderNumber" value="<?php echo $order->get_order_number(); ?>">
							<label for="subscribers">
								IOG Subscriptions for:
								<span>Provide a name and email for each subscription, one per line.</span>
							</label>
							<textarea id="subscribers" name="subscribers" placeholder="John Doe, john@website.com&#10;Jane Doe, jane@website.com"></textarea>
							<p>
								<span>Example</span>
								John Doe, john@website.com<br>Jane Doe, jane@website.com
							</p>
							<button type="submit" value="Submit Subscribers">Submit Subscribers</button>
						</form>
					</div>
				</section>
			<?php endif; ?>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>