<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>

	<?php //general stuff ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php
	/*----------------------------------------------------------------*\
	|
	|	title, social and other seo tags are all handled and inserted 
	| via The SEO Framework which is a must have plugin
	|
	\*----------------------------------------------------------------*/
	?>
	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<?php wp_head(); ?>

	<?php
		// user logged in already trying to acccess login page is redirected to account page instead
		if ( is_page(2267) && is_user_logged_in() ) { 
			wp_redirect( '/my-account');
			exit;
		} 
	?>
	
	<?php
	/*----------------------------------------------------------------*\
	|
	|	Need to exclude cookieControl and cookieControlPrefs from site cache
	|
	\*----------------------------------------------------------------*/
	?>
	<?php if( !isset($_COOKIE['cookieControlPrefs']) || $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop Google Tag Manager head code here
		| Hot Jar tracking will be added within Google Tag Manager
		|
		\*----------------------------------------------------------------*/
		?>

	<?php } ?>
	<?php if( !isset($_COOKIE['cookieControlPrefs']) || $_COOKIE['cookieControlPrefs'] == '[\"marketing\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop any marketing tracking pixels here
		| These are related to paid advertising like Google Ads
		| and Facebook ad pixels.
		|
		\*----------------------------------------------------------------*/
		?>

	<?php } ?>
	<!-- AddEvent script -->
	<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
</head>

<body <?php body_class(); ?>>

	<?php if( !isset($_COOKIE['cookieControlPrefs']) || $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
		<?php
		/*----------------------------------------------------------------*\
		|
		|	Drop Google Tag Manager body code here
		|
		\*----------------------------------------------------------------*/
		?>
		
	<?php } ?>

	<a id="skip-to-content" href="#main-content">Skip to main content</a>

	<?php get_template_part('template-parts/icon-set'); ?>