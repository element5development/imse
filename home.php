<?php 
/*----------------------------------------------------------------*\

	DEFAULT POST ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
</header>

<main id="main-content">
	<article>
		<?php if (get_field($post_type.'_intro','options')) : ?>
			<section class="is-narrow">
				<p><?php the_field($post_type.'_intro','options'); ?></p>
			</section>
		<?php endif; ?>
		<?php if (have_posts()) : ?>
			<section class="is-narrow">
				<?php	while ( have_posts() ) : the_post(); ?>
					<article class="archive-result <?php echo $post_type; ?>">
						<header>
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
						</header>
						<div class="entry-content">
							<?php the_excerpt(); ?>
						</div>
					</article>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>