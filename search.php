<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head columns-1">
	<h1><?php echo 'Search results for: ' . get_search_query(); ?></h1>
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-background.jpg">
</header>

<main id="main-content">
	<?php if (have_posts()) : ?>
		<section class="card-grid text-cards is-extra-wide columns-3">
			<?php	while ( have_posts() ) : the_post(); ?>
				<div class="card">
					<h2><?php the_title(); ?></h2>
					<div class="description">
						<?php relevanssi_the_excerpt(); ?>
					</div>	
					<div>
						<a class="button is-paint" href="<?php the_permalink(); ?>">
							Continue Reading
						</a>
					</div>
				</div>
			<?php endwhile; ?>
		</section>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<p>We cannot find anything for "<?php echo(get_search_query()); ?>".</p>
				<?php get_search_form( $echo ); ?>
			</section>
		</article>
	<?php endif; ?>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>