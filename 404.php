<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="action-header">
		<h1>Page Not Found</h1>
		<p class="subheader">We can't find this page. It might be an old link or maybe it moved.</p>
		<svg viewBox="0 0 1440 320">
			<use xlink:href="#wave"></use>
		</svg>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-background.jpg" />
	</header>

<main id="main-content">
	<article>
		</section>
		<section class="card-grid text-cards is-extra-wide columns-3">
			<div class="card">
				<h2>Products & Training</h2>
				<div class="description">
					<p>Looking for educational materials, supplies, resources, or training?</p>
				</div>	
				<div>
					<a class="button" href="">
						View Products
					</a>
					<a class="button is-text" href="">
						Upcoming Training Sessions
					</a>
				</div>
			</div>
			<div class="card">
				<h2>About Us</h2>
				<div class="description">
					<p>We are teachers, and we know how you feel. Learn about our mission and discover the values we practice.</p>
				</div>	
				<div>
					<a class="button" href="">
						Read More
					</a>
					<a class="button is-text" href="">
						Contact Us Directly
					</a>
				</div>
			</div>
			<div class="card">
				<h2>Search</h2>
				<div class="description">
					<p>The fastest way to find the content your looking for is by searching for a keyword or phrase.</p>
				</div>	
				<div>
					<?php get_search_form(); ?>
				</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>